#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

int *counter;

void program_terminated(int i)
{
  *counter=*counter-1;
  printf ("Terminating\n");
  exit(1);
}

int main()
{   
    unsigned char temp_hash = 0; 
    size_t mem_size = 1000001+4;
    char pathname[] = "a.c"; 

    struct sigaction sa;
    sa.sa_handler = program_terminated;
    sigaction(SIGINT, &sa, 0);

    key_t key = ftok(pathname, 0); //ключ для общей памяти
    if (key < 0)
    {
        printf("Can\'t generate key\n");
        exit(-1);
    } 
     
    int shmid = shmget(key,mem_size,0666 | IPC_CREAT | IPC_EXCL); //дескриптор 
    if (shmid < 0)
    {

        if (errno != EEXIST)
        {
            printf("Can\'t create shared memory\n");
            exit(-1);
        }
        else
        {

            if ((shmid = shmget(key,mem_size, 0)) < 0)
            {
                printf("Can\'t find shared memory\n");
                exit(-1);
            }
        }
    }

    void *shared_mem = shmat(shmid, NULL, 0); //ptr на общую память
    if (shared_mem  == (int *)(-1))
    {
        printf("Can't attach shared memory\n");
        exit(-1);
    }

    int *array = (int *)shared_mem;
    unsigned char *hash = array + 250000+1; //сдвиг до ласт элемента - хеша 
    counter = array + 250000;
  
  
    printf("%d",*counter);
    *counter=*counter+1;
    printf("%d",*counter);
    if (*counter == 1)
    {
        while(1)
        {   
            temp_hash = 0;
            for(size_t i=0;i<250000;++i)
            {
                array[i]=rand()%10;
                temp_hash+=array[i];

            }
            *hash = temp_hash;
            printf("HASH: %d \n",temp_hash);
            usleep(100000); // милисекунды

        }


    }


    if (shmdt(array) < 0)
    {
        printf("Can't detach shared memory\n");
        exit(-1);
    }
    return 0;
}